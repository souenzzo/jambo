(ns jambo.testing
  (:require [jequitiba.datastore.testing :as ds.testing]
            [jambo.server :as jambo]
            [io.pedestal.test :as pedestal.test]
            [jequitiba.datastore :as ds]
            [com.wsscode.pathom.graphql :as pgql]
            [io.pedestal.http :as http]
            [clojure.spec.test.alpha :as stest]
            [jambo.twilio :as twilio]
            [cheshire.core :as json]
            [jequitiba.pedestal :as pedestal]
            [jequitiba.uuid :as uuid]
            [jequitiba.http :as request])
  (:import (com.google.cloud.datastore.testing LocalDatastoreHelper)
           (java.util UUID)))

(set! *warn-on-reflection* true)

(stest/instrument)

(defonce test-datastore
         (delay
           (ds.testing/local-datastore)))

(defn ->app
  [& {::keys [responses]
      :as    ks}]
  (let [responses (atom responses)
        requests (atom [])
        datastore ^LocalDatastoreHelper @test-datastore
        app (assoc (into jambo/service ks)
              ::request/driver (fn [request]
                                 (swap! requests conj request)
                                 (ffirst (swap-vals! responses rest)))
              ::uuid/uuid-gen (uuid/sequential-uuid)
              ::requests requests
              ::twilio/token "my-token"
              ::twilio/account "my-account"
              ::datastore datastore
              ::ds/port (.getPort datastore)
              ::ds/project-id (str (UUID/randomUUID)))]
    (-> (assoc app
          ::ds/datastore (ds/default-service app))
        pedestal/default-interceptors
        http/create-server)))

(defn graphql
  [{::http/keys [service-fn]} query]
  (let [query (cond
                (vector? query) (pgql/query->graphql query)
                :else query)
        {:keys [body]
         :as   response} (pedestal.test/response-for
                           service-fn :post "/graphql"
                           :headers {"Content-Type" "application/graphql"}
                           :body query)]
    (try
      (let [data (json/parse-string body keyword)]
        (if (contains? data :errors)
          (assoc data ::query query)
          data))
      (catch Throwable e
        (println e)
        response))))