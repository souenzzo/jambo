(ns jambo.server-test
  (:require [jambo.server :as jambo]
            [midje.sweet :refer :all]
            [jambo.testing :as test]
            [jequitiba.http :as http]
            [clojure.test :refer [deftest]]
            [jambo.twilio :as twilio]))

(deftest create-task
  (let [app (test/->app)]
    (fact
      (jambo/new-task app {:my.task/id          "001"
                           :my.task/description "Abacate"})
      => {:my.task/description "Abacate"
          :my.task/id          "001"})
    (fact
      (test/graphql app [:hello])
      => {:data {:hello "world"}})
    (fact
      "All my tasks"
      (test/graphql app `[{:tasks [:id :description]}])
      => {:data {:tasks [{:description "Abacate" :id "001"}]}})
    (fact
      (test/graphql app `[{(create_task {:description "abc"}) [:id :description]}])
      => {:data {:create_task {:description "abc"
                               :id          "00000000-0000-0000-0000-000000000000"}}})
    (fact
      "All my tasks with a new one"
      (test/graphql app `[{:tasks [:id :description]}])
      => {:data {:tasks [{:description "abc"
                          :id          "00000000-0000-0000-0000-000000000000"}
                         {:description "Abacate"
                          :id          "001"}]}})))


(deftest send-sms
  (let [app (test/->app
              ::test/responses [{:body   {:sid  "my-sid"
                                          :body "hello!"}
                                 :status 200}])]
    (fact
      "Sending sms"
      (test/graphql app `[{(send_sms {:text "hello"}) [:id :text]}])
      => {:data {:send_sms {:id   "my-sid"
                            :text "hello!"}}})
    (fact
      "Checking requests"
      @(::test/requests app)
      => [{::http/form-params {:from "me"
                               :text "hello"
                               :to   "you"}
           ::http/method      ::http/post
           ::http/password    "my-token"
           ::http/url         "https://api.twilio.com/2010-04-01/Accounts/my-account/Messages.json"
           ::http/username    "my-account"}])))


(deftest send-sms-with-error
  (let [app (test/->app
              ::test/responses [{:status 403}])]
    (fact
      "Sending sms"
      (test/graphql app `[{(send_sms {:text "hello"}) [:id :text]}])
      => (contains {:status 500}))
    (fact
      "Checking requests"
      @(::test/requests app)
      => [{::http/form-params {:from "me"
                               :text "hello"
                               :to   "you"}
           ::http/method      ::http/post
           ::http/password    "my-token"
           ::http/url         "https://api.twilio.com/2010-04-01/Accounts/my-account/Messages.json"
           ::http/username    "my-account"}])))
