(ns jequitiba.uuid
  (:import (java.util UUID)))

(defprotocol IUUIDGen
  (-next-uuid [uuid-gen]))

(def system-random-uuid
  (reify IUUIDGen
    (-next-uuid [_]
      (UUID/randomUUID))))

(defn random-uuid
  [{::keys [uuid-gen]}]
  (-next-uuid uuid-gen))

(defn sequential-uuid
  []
  (let [uuids (atom (for [i (range)]
                      (new UUID 0 (long i))))]
    (reify IUUIDGen
      (-next-uuid [_]
        (ffirst (swap-vals! uuids rest))))))
