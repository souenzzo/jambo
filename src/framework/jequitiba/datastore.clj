(ns jequitiba.datastore
  (:refer-clojure :exclude [key])
  (:require [clojure.spec.alpha :as s])
  (:import (com.google.cloud.datastore Key DatastoreOptions Datastore KeyFactory Entity Entity$Builder)))

(set! *warn-on-reflection* true)

(defn datastore?
  [x]
  (instance? Datastore x))

(defn entity?
  [x]
  (instance? Entity x))

(defn key?
  [x]
  (instance? Key x))

(s/def ::datastore datastore?)
(s/def ::kind string?)
(s/def ::key-name string?)
(s/def ::key key?)
(s/def ::port pos?)
(s/def ::project-id string?)
(s/def ::entity entity?)


(defn default-service
  ^Datastore [{::keys [port project-id]}]
  (.getService (-> (doto (DatastoreOptions/newBuilder)
                     (.setHost (format "localhost:%s" port))
                     (.setProjectId project-id))
                   .build)))

(defn service
  ^Datastore [{::keys [datastore]}]
  datastore)

(s/fdef service
        :args (s/cat :app (s/keys :req [::datastore]))
        :ret ::datastore)

(defn key
  ^Key [{::keys [kind
                 ^String key-name]
         :as    app}]
  (let [datastore (service app)]
    (-> (.newKeyFactory datastore)
        ^KeyFactory (.setKind kind)
        (.newKey key-name))))

(s/fdef key
        :args (s/cat :app (s/keys :req [::kind
                                        ::key-name]))
        :ret ::key)

(defn entity
  ^Entity [^Key key {:as kvs}]
  (let [e (Entity/newBuilder key)]
    (-> (fn [^Entity$Builder e k ^String v]
          (.set e (name k) v))
        ^Entity$Builder (reduce-kv e kvs)
        (.build))))

(s/fdef entity
        :args (s/cat :key ::key
                     :kvs map?)
        :ret ::entity)

(defprotocol IClj
  (->clj [this]))

(extend-protocol IClj
  Key
  (->clj [this]
    (let [k (keyword (.getKind this) "id")]
      {k (.getName this)}))
  Entity
  (->clj [this]
    (let [names (.getNames this)
          key (.getKey this)
          keymap (->clj key)
          ns (-> keymap first first namespace)]
      (-> (fn [acc name]
            (assoc acc (keyword ns name)
                       (.get (.getValue this name))))
          (reduce keymap names)))))
