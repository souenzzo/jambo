(ns jequitiba.datastore.testing
  (:import (com.google.cloud.datastore.testing LocalDatastoreHelper)))

(defn local-datastore
  []
  (doto (LocalDatastoreHelper/create)
    (.start)))
