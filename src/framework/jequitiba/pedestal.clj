(ns jequitiba.pedestal
  (:require [io.pedestal.http :as http]
            [com.walmartlabs.lacinia.pedestal :as lp]))

(defn default-interceptors
  [{:keys  [env]
    ::keys [schema]
    :as    service-map}]
  (let [dev? (= env :dev)
        service-map (merge (lp/service-map schema {:app-context service-map})
                           service-map)]
    (cond-> service-map
            dev? http/dev-interceptors)))
