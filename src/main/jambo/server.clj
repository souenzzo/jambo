(ns jambo.server
  (:require [io.pedestal.http :as http]
            [jequitiba.datastore :as ds]
            [jequitiba.pedestal :as pedestal]
            [jequitiba.uuid :as uuid]
            [jequitiba.http :as request]
            [jambo.twilio :as twilio]
            [com.walmartlabs.lacinia.schema :as schema])
  (:import (com.google.cloud.datastore Query)))

(set! *warn-on-reflection* true)

(defn new-task
  [app {:my.task/keys [id description]}]
  (let [datastore (ds/service app)
        ident (ds/key (assoc app
                        ::ds/kind "my.task"
                        ::ds/key-name id))
        entity (ds/entity ident {:my.task/description description})]
    (.put datastore entity)
    (ds/->clj (.get datastore ident))))

(defn create-task
  [app {:keys [description]} parent]
  (new-task app {:my.task/id          (str (uuid/random-uuid app))
                 :my.task/description description}))

(defn my-tasks
  [app _ _]
  (let [datastore (ds/service app)
        query (-> (Query/newEntityQueryBuilder)
                  (.setKind "my.task")
                  (.build))]
    (->> (.run datastore query)
         (iterator-seq)
         (map ds/->clj))))
(defn resolve-key
  [k]
  (fn [_ _ entity]
    (get entity k)))

(defn send-sms
  [app {:keys [text]} _]
  (let [request (twilio/send-sms app {::twilio/body text
                                      ::twilio/from "me"
                                      ::twilio/to   "you"})
        response (request/request app request)
        {::twilio/keys [sid body]} (twilio/->message app response)]
    {:my.sms/id   sid
     :my.sms/text body}))

(def hello-schema
  (schema/compile
    {:objects   {:sms     {:fields {:id   {:type    'ID
                                           :resolve (resolve-key :my.sms/id)}
                                    :text {:type    'String
                                           :resolve (resolve-key :my.sms/text)}}}
                 :my_task {:fields {:id          {:type    'ID
                                                  :resolve (resolve-key :my.task/id)}
                                    :description {:type    'String
                                                  :resolve (resolve-key :my.task/description)}}}}
     :mutations {:send_sms    {:type    :sms
                               :args    {:text {:type 'String}}
                               :resolve send-sms}
                 :create_task {:type    :my_task
                               :args    {:description {:type 'String}}
                               :resolve create-task}}
     :queries   {:tasks {:type    '(list :my_task)
                         :resolve my-tasks}
                 :hello {:type    'String
                         :resolve (constantly "world")}}}))

(def service
  {::pedestal/schema hello-schema
   ::uuid/uuid-gen   uuid/system-random-uuid
   :env              :prod})

(defn start!
  [st service]
  (when st
    (http/stop st))
  (-> service
      pedestal/default-interceptors
      http/create-server
      http/start))

(defonce app (atom nil))

(defn dev-main
  [& _]
  (swap! app start! (assoc service :env :dev)))

(defn -main
  [& _]
  (swap! app start! service))
