(ns jambo.twilio
  (:require [jequitiba.http :as http]))


(defn send-sms
  [{::keys [account token]} {::keys [body from to]}]
  {::http/url         (format "https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json"
                              account)
   ::http/username    account
   ::http/password    token
   ::http/method      ::http/post
   ::http/form-params {:from from
                       :to   to
                       :text body}})

(defn ->message
  [app {{:keys [sid body]} :body
        :keys [status]}]
  (if  (= status 200)
    {::sid  sid
     ::body body}
    (throw (ex-info (str "twilio fails with " status)
                    body))))

